from data_structures import Item

class Hypergraph(object):


    def __init__(self):
        pass

    def siblings(self):
        pass


class LazyEisner(Hypergraph):


    def siblings(self):
        pass


class LazyArcEager(Hypergraph):

    def outgoing(self, item):
        pass


class LazyArcStandard(Hypergraph):


    def __init__(self, n, chart, W):
        self.n = n
        self.chart = chart
        self.W = W
    
        
    def outgoing(self, item):
        """ Lazily Expand the Hypergraph """
        i, j, h = item.i, item.j, item.h
        # items to the left
        for k in range(0, i+1):
            for g in range(k, i):
                if (k, i, g) in self.chart:
                    item_l = self.chart[(k, i, g)]
                    p = item_l.w * item.w
                    # attach left arc
                    yield Item(k, j, g, p*self.W[(h, g)], item_l, item)
                    # attach right arc
                    yield Item(k, j, h, p*self.W[(g, h)], item_l, item)
                            
        # items to the right
        for k in range(j, self.n+1):
            for g in range(j, k):
                if (j, k, g) in self.chart:
                    item_r = self.chart[(j, k, g)]
                    p = item.w * item_r.w
                    # attach left arc
                    yield Item(i, k, h, p*self.W[(g, h)], item, item_r)
                    # attach right arc
                    yield Item(i, k, g, p*self.W[(h, g)], item, item_r)
    
    
