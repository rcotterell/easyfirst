from data_structures import Item, Agenda, Chart
from hypergraph import LazyArcStandard
import numpy as np
import unittest
from collections import defaultdict
np.random.seed(8675309)

class Parser(object):

    def __init__(self):
        pass
    
    def parse_agenda(self, sentence, prune=False):
        
        # set-up
        n = len(sentence)
        W = {}
        for g in range(n+1):
            for h in range(n+1):
                W[(g, h)] = np.exp(np.random.randn())
        
        # data structures
        agenda = Agenda()
        chart = Chart()
        hypergraph = LazyArcStandard(n, chart, W)
        bucket = defaultdict(lambda: 0)

        
        # initialize
        for i in range(n):
            agenda[(i, i+1, i)] = \
                Item(i, i+1, i, W[(i+1, i)], i, i)
        
        # algorithm
        item = None
        pops = 0
        popped = defaultdict(lambda: 0)

        while not agenda.empty():
            item = agenda.pop()

            ### PRUNING BLOCK
            if prune:
                if item.l in bucket or item.r in bucket:
                    continue
                bucket[item.l] += 1
                bucket[item.r] += 1
            ### PRUNING BLOCK

            # add to chart
            chart[item] = item

            # count the pops
            popped[(item.i, item.j, item.h)] += 1
            pops += 1

            # main loop
            for item_new in hypergraph.outgoing(item):
                agenda[(item_new.i, item_new.j, item_new.h)] = item_new


        print("# pops:\t{0}".format(pops))
        return popped


    def parse_cky(self, sentence):
        n = len(sentence)

        # initialize
        
        
        # main loop
        for i in range(n+1):
            for j in range(i+1, n+1):
                for h1 in range(i, j): 
                    for k in range(j+1, n+1):
                        for h2 in range(j, k):
                            # sanity checks
                            assert i < j
                            assert j < k
                            assert h1 >= i
                            assert h2 >= j
                            assert h1 < j
                            assert h2 < k
                            pass

                        
    def parse_goldberg(self, sentence):
        n = len(sentence)
        pending = range(n)
        popped = []
        
        
        
    
class TestAgenda(unittest.TestCase):

    def test_all_spans(self):
        sentence = ['Papa', 'ate', 'the', 'caviar', 'with', 'a', 'spoon', '.']
        n = len(sentence)
        main = Parser()
        #main.parse_agenda(sentence)
        #self.parse_cky(sentence)
        
        popped = main.parse_agenda(sentence)
        
        for i in range(n+1):
            for j in range(i+1, n+1):
                for h in range(i, j):
                    self.assertTrue((i, j, h) in popped)
                    self.assertEqual(popped[(i, j, h)], 1)

        
if __name__ == '__main__':
    unittest.main()
    #sentence = ['Papa', 'ate', 'the', 'caviar', 'with', 'a', 'spoon', '.']
    #sentence = ['Papa', 'ate', 'the']
    #n = len(sentence)
    #main = Parser()
    #main.parse_agenda(sentence)
    #popped = main.parse_agenda(sentence)
    #exit(0)


